﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4T.Services.ParserClass
{
    public class UserAccuntSetting
    {
        public Settings settings { get; set; }
        public SettingPossibleValue settingPossibleValue { get; set; }
    }
   public class SettingPossibleValue
   {
       public int settingPossibleValueId { get; set; }
       public string possibleValue { get; set; }
   }

   public class Settings
   {
       public int settingId { get; set; }
       public List<SettingPossibleValue> settingPossibleValues { get; set; }
       public string parameterName { get; set; }
   }

   //public class SettingPossibleValue2
   //{
   //    public int settingPossibleValueId { get; set; }
   //    public string possibleValue { get; set; }
   //}

   
}
