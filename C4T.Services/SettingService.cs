﻿using C4T.Services.ParserClass;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace C4T.Services
{
   public class SettingService
    {

       public static void GetSettingRequest(string _email,string _password)
       {
           try
           {
               HttpWebRequest _setting = HttpWebRequest.CreateHttp("http://192.168.101.137:8181/mhm-web/api/accountSetting/");
               _setting.Accept = "application/json";
               _setting.ContentType = "application/json";
               _setting.Headers["username"] = _email;
               _setting.Headers["password"] = _password;
               _setting.Method = "GET";
               _setting.BeginGetResponse(new AsyncCallback(new SettingService().SettingRequest), _setting);
               
           }
           catch (Exception ex)
           {
           }
       }

       private void SettingRequest(IAsyncResult asyncResult)
       {
           try
           {
               MemoryStream ms;
               DataContractJsonSerializer _responseData;
               HttpWebRequest request = (HttpWebRequest)asyncResult.AsyncState;
               HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asyncResult);
               HttpWebResponse httpResponse = (HttpWebResponse)response;
               string _responestring = null;
               using (Stream data = response.GetResponseStream())
               using (var reader = new StreamReader(data))
               {

                   _responestring = reader.ReadToEnd();
                   using (ms = new MemoryStream(Encoding.Unicode.GetBytes(_responestring)))
                   {
                       _responseData = new DataContractJsonSerializer(typeof(UserAccuntSetting));
                       UserAccuntSetting _loginObj = _responseData.ReadObject(ms) as UserAccuntSetting;
                   }
               }

           }
           catch (WebException e)
           {
               using (WebResponse response = e.Response)
               {
                   HttpWebResponse httpResponse = (HttpWebResponse)response;
                   using (Stream data = response.GetResponseStream())
                   using (var reader = new StreamReader(data))
                   {
                       string text = reader.ReadToEnd();
                   }
               }
           }
       }
    }
}
