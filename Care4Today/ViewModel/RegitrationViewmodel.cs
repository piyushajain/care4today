﻿using Care4Today.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace Care4Today.ViewModel
{
    class RegitrationViewmodel
    {
        public static int InserRegistration(user obj)
        {
            try
            {
                return App.OpenDB().Insert(obj);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        public static bool IsValidEmail(string strIn)
        {
            return Regex.IsMatch(strIn,
             @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
            @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|([0-9a-zA-Z][-\w]*[0-9a-zA-Z]))+(.com|.edu|.net|.mil|.co.in|.in)$");
        }


        public static bool IsValidanswer(string str)
        {
            return Regex.IsMatch(str, @"[^0-9]");
        }
        public static bool IsValidUser(string str)
        {
            return Regex.IsMatch(str, @"[0-9a-zA-Z]");
        }

        public static bool IsValidPassword(string str)
        {
            //[^-!@#\$%()&\;'\*\+/=\?\^`\{\}\]
            ////[?-~!@&#\$%\^\*()_+{}:|""?`;',./[\]\\\0-9\\\a-zA-Z]*
            //return Regex.IsMatch(str, @"[^-!@#\$%()&\;'\*\+/=\?\^`\{\}\][^0-9][^a-zA-Z][?a-zA-Z|0-9|-~!@&#\$%\^\*()_+{}:|""?`;',./[\]]*");
            return Regex.IsMatch(str, @"[^0-9a-zA-Z]");
        }
        public static int GetUserID(string _username)
        {
            int _id = 0;
            try
            {
                _id = App.OpenDB().Table<user>().Where(x => x.email == _username).ToList().FirstOrDefault().user_id;
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
                throw ex;
            }
            return _id;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_email"></param>
        /// <returns></returns>
        public static int UpdateRegister(user _registerObj)
        {
            try
            {
                int _updateCount = 0;
                _updateCount = App.OpenDB().Update(_registerObj);
                return _updateCount;
            }
            catch (SQLiteException ex)
            {
                return 0;
            }
        }
        /// <summary>
        /// Get User Detail according to Email
        /// </summary>
        /// <returns></returns>
        public static user GetUserDetail(string _email)
        {
            user _ragister = new user();
            try
            {
                _ragister = App.OpenDB().Table<user>().Where(x => x.email == _email).ToList().FirstOrDefault();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return _ragister;
        }

        /// <summary>
        /// Get User Avaiable Locally With Email and password
        /// </summary>
        /// <returns></returns>
        public static int ValidatUser(string _email, string _password)
        {
            int _count = 0;
            try
            {
                _count = App.OpenDB().Table<user>().Where(x => x.email == _email && x.password == _password).Count();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return _count;
        }
        /// <summary>
        /// Get Last active User Session Time
        /// </summary>
        /// <returns></returns>
        public static user GetKeepMeLoginTime()
        {
            user _ragister = new user();
            try
            {
                _ragister = (from _ragisterObj in App.OpenDB().Table<user>()
                             where _ragisterObj.IsActive == "Yes"
                             group _ragisterObj by _ragisterObj.IsActive into g
                             select new user()
                             {
                                 KeepMeLoginTime = g.FirstOrDefault().KeepMeLoginTime
                                 ,
                                 KeepMeLoginOption = g.FirstOrDefault().KeepMeLoginOption
                             }).FirstOrDefault();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return _ragister;
        }


    }
}

