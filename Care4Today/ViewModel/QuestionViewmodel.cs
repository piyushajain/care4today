﻿using Care4Today.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Care4Today.ViewModel
{
    public class QuestionViewmodel
    {
        public static List<SecurityQuestion> selectAll()
        {
            try
            {
                List<SecurityQuestion> lst = App.OpenDB().Table<SecurityQuestion>().ToList();
                return lst;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
    }
}