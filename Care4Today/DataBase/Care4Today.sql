DROP TABLE IF EXISTS "SecurityQuestion";
CREATE TABLE "SecurityQuestion" ("ID" INTEGER PRIMARY KEY  NOT NULL , "Question" TEXT);
DROP TABLE IF EXISTS "alert";
CREATE TABLE alert (
alert_id integer  NOT NULL  PRIMARY KEY AUTOINCREMENT,
description Varchar(200) DEFAULT NULL,
alert_type Varchar(50)  NOT NULL,
status integer  NOT NULL DEFAULT 0
);
DROP TABLE IF EXISTS "charity";
CREATE TABLE charity (charityId integer  PRIMARY KEY DEFAULT NULL,charity_name Varchar(100) DEFAULT NULL,status Varchar(50),charity_image_url Varchar(150),charity_status Varchar(50),amount Double);
DROP TABLE IF EXISTS "familyRX";
CREATE TABLE familyRX (member_id INTEGER  PRIMARY KEY DEFAULT NULL,member_name Varchar(100)  NOT NULL,member_alert_type Varchar(150) DEFAULT 0,member_picture_name Varchar(50) DEFAULT NULL,member_picture_url Varchar(150) DEFAULT NULL,member_adherence INTEGER DEFAULT NULL,request_status Varchar(150) DEFAULT NULL,member_email Varchar(150) DEFAULT NULL,member_phone integer);
DROP TABLE IF EXISTS "medication";
CREATE TABLE medication (medi_server_id INTEGER  NOT NULL  PRIMARY KEY DEFAULT 0,drug_id INTEGER NOT NULL,nick_name Varchar(100) DEFAULT NULL,dose Float NOT NULL,dose_unit Varchar(15) NOT NULL,frequency Smallint  NOT NULL DEFAULT 0,start_date Double DEFAULT NULL,specific_datetime Varchar DEFAULT NULL,interval INTEGER NOT NULL,week_day Varchar(13) DEFAULT NULL,day_of_month INTEGER DEFAULT NULL,reminder_msg Varchar(200) DEFAULT NULL,is_refill_alert Varchar(1) NOT NULL DEFAULT 'N',is_image_uploaded smallint NOT NULL DEFAULT 0,img_name Varchar(50) DEFAULT NULL,img_url Varchar(150) DEFAULT NULL,timezone Varchar(3) NOT NULL,created_datetime Double  DEFAULT NULL,CONSTRAINT "fk_drug_id" FOREIGN KEY ("drug_id") REFERENCES "user_drug" ("drug_id"));
INSERT INTO "medication" VALUES(47,47,'ncik1',5,'Puff',2,1368685654366,'47700000',1,NULL,0,'Time to take','Y',0,'','','Asia/Kolkata',1368685654366);
INSERT INTO "medication" VALUES(48,45226,'nick',0.5,'Tablet',0,0,NULL,0,NULL,NULL,'Time to take your medication','Y',0,'DrugItem_11963.JPG',NULL,'Asia/Kolkata',NULL);
DROP TABLE IF EXISTS "refill";
CREATE TABLE refill (
refill_id INTEGER  PRIMARY KEY NOT NULL,
medi_server_id INTEGER NOT NULL,
pharmacy Varchar(100) DEFAULT NULL,
pharmacy_contact Varchar(20) DEFAULT NULL,
reminder_msg Varchar(200) DEFAULT NULL,
refill_type Varchar(1)  NOT NULL,
current_qty INTEGER DEFAULT 0,
refill_qty INTEGER DEFAULT 0,
last_refill_date Double DEFAULT 0,
refill_interval Smallint DEFAULT 0,
remind_me Smallint DEFAULT 3,
status Smallint  NOT NULL DEFAULT 0,
CONSTRAINT "re_fk_medi_server_id" FOREIGN KEY ("medi_server_id") REFERENCES "medication" ("medi_server_id") ON DELETE CASCADE);
INSERT INTO "refill" VALUES(17,47,NULL,NULL,'Time to refill your prescription','D',0,0,1368772500000,32,3,0);
INSERT INTO "refill" VALUES(18,48,'himanshi goyal','(958) 999-9999','Timeee ','Q',20,10,NULL,NULL,3,0);
DROP TABLE IF EXISTS "schedule_occurrences";
CREATE TABLE schedule_occurrences(
occurrence_id INTEGER NOT NULL,
medi_server_id INTEGER DEFAULT NULL,
medi_sch_id INTEGER NOT NULL,
occ_datetime Double NOT NULL,
occ_datetime_str Varchar(20) NOT NULL,
is_scheduled Varchar(1) NOT NULL DEFAULT 'N',
status Smallint  NOT NULL DEFAULT 0,
status_datetime Double DEFAULT NULL,
status_timezone Varchar(40) DEFAULT NULL,
is_synched Varchar(1) NOT NULL,
 CONSTRAINT "occfk_medi_server_id" FOREIGN KEY ("medi_server_id") REFERENCES "medication" ("medi_server_id"),
PRIMARY Key ("occurrence_id",
 "medi_server_id",
 "medi_sch_id"));
INSERT INTO "schedule_occurrences" VALUES(1,47,1,1368690300000,'2013-05-16 13:15:00','Y',0,NULL,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(2,47,1,1368776700000,'2013-05-17 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(3,47,1,1368863100000,'2013-05-18 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(4,47,1,1368949500000,'2013-05-19 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(5,47,1,1369035900000,'2013-05-20 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(6,47,1,1369122300000,'2013-05-21 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(7,47,1,1369208700000,'2013-05-22 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(8,47,1,1369295100000,'2013-05-23 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(9,47,1,1369381500000,'2013-05-24 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(10,47,1,1369467900000,'2013-05-25 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(11,47,1,1369554300000,'2013-05-26 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(12,47,1,1369640700000,'2013-05-27 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(13,47,1,1369727100000,'2013-05-28 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(14,47,1,1369813500000,'2013-05-29 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(15,47,1,1369899900000,'2013-05-30 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(16,47,1,1369986300000,'2013-05-31 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(17,47,1,1370072700000,'2013-06-01 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(18,47,1,1370159100000,'2013-06-02 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(19,47,1,1370245500000,'2013-06-03 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(20,47,1,1370331900000,'2013-06-04 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(21,47,1,1370418300000,'2013-06-05 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(22,47,1,1370504700000,'2013-06-06 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(23,47,1,1370591100000,'2013-06-07 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(24,47,1,1370677500000,'2013-06-08 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(25,47,1,1370763900000,'2013-06-09 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(26,47,1,1370850300000,'2013-06-10 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(27,47,1,1370936700000,'2013-06-11 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(28,47,1,1371023100000,'2013-06-12 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(29,47,1,1371109500000,'2013-06-13 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(30,47,1,1371195900000,'2013-06-14 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(31,47,1,1371282300000,'2013-06-15 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(32,47,1,1371368700000,'2013-06-16 13:15:00','Y',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(33,47,1,1371455100000,'2013-06-17 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(34,47,1,1371541500000,'2013-06-18 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(35,47,1,1371627900000,'2013-06-19 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(36,47,1,1371714300000,'2013-06-20 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(37,47,1,1371800700000,'2013-06-21 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(38,47,1,1371887100000,'2013-06-22 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(39,47,1,1371973500000,'2013-06-23 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(40,47,1,1372059900000,'2013-06-24 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(41,47,1,1372146300000,'2013-06-25 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(42,47,1,1372232700000,'2013-06-26 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(43,47,1,1372319100000,'2013-06-27 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(44,47,1,1372405500000,'2013-06-28 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(45,47,1,1372491900000,'2013-06-29 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(46,47,1,1372578300000,'2013-06-30 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(47,47,1,1372664700000,'2013-07-01 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(48,47,1,1372751100000,'2013-07-02 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(49,47,1,1372837500000,'2013-07-03 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(50,47,1,1372923900000,'2013-07-04 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(51,47,1,1373010300000,'2013-07-05 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(52,47,1,1373096700000,'2013-07-06 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(53,47,1,1373183100000,'2013-07-07 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(54,47,1,1373269500000,'2013-07-08 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(55,47,1,1373355900000,'2013-07-09 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(56,47,1,1373442300000,'2013-07-10 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(57,47,1,1373528700000,'2013-07-11 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(58,47,1,1373615100000,'2013-07-12 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(59,47,1,1373701500000,'2013-07-13 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(60,47,1,1373787900000,'2013-07-14 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(61,47,1,1373874300000,'2013-07-15 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(62,47,1,1373960700000,'2013-07-16 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(63,47,1,1374047100000,'2013-07-17 13:15:00','N',0,0,NULL,'N');
INSERT INTO "schedule_occurrences" VALUES(64,47,1,1374133500000,'2013-07-18 13:15:00','N',0,0,NULL,'N');
DROP TABLE IF EXISTS "system_parameter";
CREATE TABLE system_parameter (sys_param_id integer  PRIMARY KEY AUTOINCREMENT DEFAULT NULL,param_name VARCHAR(100),value VARCHAR(50));
INSERT INTO "system_parameter" VALUES(1,'CARE4CHARITY','ON');
INSERT INTO "system_parameter" VALUES(2,'HEALTH_TIPS','OFF');
INSERT INTO "system_parameter" VALUES(3,'CARE4CHARITY','ON');
INSERT INTO "system_parameter" VALUES(4,'HEALTH_TIPS','OFF');
DROP TABLE IF EXISTS "user";
CREATE TABLE user (user_id integer  PRIMARY KEY DEFAULT NULL,user_name Varchar(100)  NOT NULL,email Varchar(100)  NOT NULL,password Varchar(20)  NOT NULL,security_question Varchar(500)  NOT NULL,answer Varchar(100)  NOT NULL,profile_pic_url Varchar(150) DEFAULT NULL,profile_pic_name Varchar(150) DEFAULT NULL,phone Varchar(20) DEFAULT NULL,phone_type Varchar(20) DEFAULT NULL,locale_id integer,locale_name Varchar(50) DEFAULT NULL,locale_description Varchar(100) DEFAULT NULL,source_id integer,source_name Varchar(20) DEFAULT NULL);
DROP TABLE IF EXISTS "user_drug";
CREATE TABLE user_drug(
drug_id INTEGER  NOT NULL,
drug_name Varchar(100)  NOT NULL,
drug_brand_name Varchar(50)  DEFAULT NULL,
strength Varchar(10)  NOT NULL,
drug_type Varchar(15) NOT NULL,
is_customized smallint NOT NULL DEFAULT 0,
CONSTRAINT "pk_drug_id" PRIMARY KEY ("drug_id",
 "is_customized"));
INSERT INTO "user_drug" VALUES(47,'medi',NULL,'Yuit','Spray',1);
INSERT INTO "user_drug" VALUES(45226,'Acarbose ',NULL,'25.0mg','Tablet',0);
DROP TABLE IF EXISTS "user_settings";
CREATE TABLE user_settings (user_setting_id integer  PRIMARY KEY DEFAULT NULL,param_name VARCHAR(100),value Boolean DEFAULT NULL);
INSERT INTO "user_settings" VALUES(1,'All Reminders',1);
INSERT INTO "user_settings" VALUES(2,'Reminder Sound',1);
INSERT INTO "user_settings" VALUES(4,'KEEP_ME_SIGN_IN',1);
INSERT INTO "user_settings" VALUES(5,'Health Tips',1);
INSERT INTO "user_settings" VALUES(55,'ALERT_SOUND_NAME',1);
