﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Text;
using System.IO;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;
using Care4Today.ViewModel;
using Microsoft.Phone.Net.NetworkInformation;
using Care4Today.Model;
using System.IO.IsolatedStorage;
using System.Windows.Resources;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace Care4Today.View.Login
{
    
    public partial class SignUp : PhoneApplicationPage
    {
        RSACryptoServiceProvider rsaProvider;
        UnicodeEncoding byteConverter;
        string Emailvalue;
        string Questionvalue;
        string Answervalue;
        string Passwordvalue;
        string UsernameValue;
        string VarifyValue;
        string uniqueID;
        List<SecurityQuestion> lstcontacts;
        string Imagevalue=string.Empty;
        string imagname = string.Empty;
        Stream filestream;
        // Constructor
        public SignUp()
        {
            InitializeComponent();

            RetriveQuestion();
            lstPickerSecqurityQue.ItemsSource = lstcontacts;
            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }
      
        public void RetriveQuestion()
        {
            lstcontacts = QuestionViewmodel.selectAll().ToList();
        }
        private void PasswordLostFocus(object sender, RoutedEventArgs e)
        {
            CheckPasswordWatermark();
        }

        public void CheckPasswordWatermark()
        {
            var passwordEmpty = string.IsNullOrEmpty(txtverify.Password);
            pwdvarify.Opacity = passwordEmpty ? 100 : 0;
            txtverify.Opacity = passwordEmpty ? 0 : 100;
        }

    
        private void PasswordGotFocus(object sender, RoutedEventArgs e)
        {
            pwdvarify.Opacity = 0;
            txtverify.Opacity = 100;
        }
        public void checkpassword()
        {
            var passwordEmpty = string.IsNullOrEmpty(txtpassword.Password);
            pwdpassword.Opacity = passwordEmpty ? 100 : 0;
            txtpassword.Opacity = passwordEmpty ? 0 : 100;
        }
        private void pwdpassword_GotFocus(object sender, RoutedEventArgs e)
        {
            pwdpassword.Opacity = 0;
            txtpassword.Opacity = 100;

        }

        private void pwdpassword_LostFocus(object sender, RoutedEventArgs e)
        {
            checkpassword();
        }
        private void showerror(string str)
        {
            txtmsg.Visibility = Visibility.Visible;
            btnclose.Visibility = Visibility.Visible;
            txtmsg.Text = str;
        }

        public static bool IsAllLettersOrDigits(string s)
        {
            foreach (char c in s)
            {
                if (!Char.IsLetter(c)||(!Char.IsDigit(c)))
                    return false;
            }
            return true;
        }
        private void btnsignup_Click(object sender, RoutedEventArgs e)
        {
           
            bool isnetwork = DeviceNetworkInformation.IsNetworkAvailable;
            if (isnetwork == true)
            {
                if (!String.IsNullOrEmpty(wtxtemail.Text))
                {
                    bool truemail = RegitrationViewmodel.IsValidEmail(wtxtemail.Text);
                    if (truemail == false)
                    {
                        string email = "Please enter valid email id";
                        showerror(email);
                        return;
                       
                    }
                    else
                    {
                        Emailvalue = wtxtemail.Text;
                        if (!string.IsNullOrEmpty(wtxtusername.Text))
                        {
                            if (wtxtusername.Text.Length < 6)
                            {
                                showerror("Username should have 6 characters");
                                return;
                            }
                            bool trueusername = RegitrationViewmodel.IsValidUser(wtxtusername.Text);
                            if (trueusername == false)
                            {
                                string username = "Please enter Valid Username";
                                showerror(username);
                                return;
                            }
                            else
                            {
                                UsernameValue = wtxtusername.Text;
                                if (!string.IsNullOrEmpty(txtpassword.Password))
                                {
                                    if (txtpassword.Password.Length < 6)
                                    {
                                        showerror("Password should have 6 to 18 characters");
                                        return;
                                    }
                                    bool truepassword=false;
                                    if ((txtpassword.Password.Any(c => char.IsDigit(c))) && (Regex.IsMatch(txtpassword.Password, @"[!@#$%^&*()_+=\[{\]};:<>|./?,\\'""-]")))
                                    {
                                        truepassword = true;
                                    }
                                    if ((txtpassword.Password.Any(c => char.IsLetter(c))) && (Regex.IsMatch(txtpassword.Password, @"[!@#$%^&*()_+=\[{\]};:<>|./?,\\'""-]")))
                                    {
                                        truepassword = true;
                                    }
                                    if ((txtpassword.Password.Any(c => char.IsLetter(c))) && (txtpassword.Password.Any(c => char.IsDigit(c))))
                                    {
                                        truepassword = true;
                                    }


                                    if (truepassword == false)
                                    {
                                        string password = "Please enter valid password";
                                        showerror(password);
                                        return;
                                    }
                                    else
                                    {
                                        Passwordvalue = txtpassword.Password;
                                        if (!string.IsNullOrEmpty(txtverify.Password))
                                        {
                                            if (txtpassword.Password != txtverify.Password)
                                            {
                                                string varify = "Password and confirmed are not matched";
                                                showerror(varify);
                                                return;
                                            }
                                            else
                                            {
                                                VarifyValue = txtverify.Password;
                                                if (lstPickerSecqurityQue.SelectedIndex == -1)
                                                {
                                                    string select = "Please select item from list";
                                                    showerror(select);
                                                    return;
                                                }
                                                else
                                                {
                                                   
                                                    Questionvalue = lstPickerSecqurityQue.SelectedItem.ToString();
                                                    if (!string.IsNullOrEmpty(txtanswer.Text))
                                                    {
                                                        bool validans = RegitrationViewmodel.IsValidanswer(wtxtanswer.Text);
                                                        if (validans == false)
                                                        {
                                                            string answer = "Please answer your selected Question";
                                                            showerror(answer);
                                                            return;
                                                        }
                                                        else
                                                        {
                                                            Answervalue = wtxtanswer.Text;
                                                          
                                                            HttpWebRequest HttpWebRequest_obj = HttpWebRequest.CreateHttp("http://192.168.101.137:8181/mhm-web/api/registration");
                                                            HttpWebRequest_obj.Accept = "application/json";
                                                            HttpWebRequest_obj.ContentType = "application/json";
                                                            HttpWebRequest_obj.Method = "POST";
                                                            HttpWebRequest_obj.BeginGetRequestStream(SignUpRequest, HttpWebRequest_obj);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        string answer = "Please enter your answer";
                                                        showerror(answer);
                                                        return;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string varify = "Please enter Confirm  password";
                                            showerror(varify);
                                            return;
                                        }
                                    }
                                }
                                else
                                {
                                    string username = "Please enter password";
                                    showerror(username);
                                    return;
                                }
                            }
                        }
                        else
                        {
                            string username = "Please enter Username";
                            showerror(username);
                            return;
                        }


                    }
                }
                else
                {
                    string email = "Please enter email id";
                    showerror(email);
                    return;
                }
            }
            else
            {
            }
        }
        private  void SignUpRequest(IAsyncResult asyncResult)
        {
            try
            {
                        string getlistvalue = string.Empty;
                        byte[] id = (byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");
                        uniqueID = Convert.ToBase64String(id);
                        string envelope = "{\"userSourceDetails\":[{\"deviceId\":\"" + uniqueID + "\",\"source\":\"windows phone\"}],\"securityQuestion\":\"" + Questionvalue + "\",\"email\":\"" + Emailvalue + "\",\"answer\":\"" + Answervalue + "\",\"phone\":\"6646456425\",\"phoneType\":\"iphone\",\"password\":\"" + Passwordvalue + "\",\"userName\":\"" + UsernameValue + "\",\"confirmPassword\":\"" + VarifyValue + "\",\"locale\":{\"localeName\":\"en_us\"},\"source\":{\"sourceName\":\"iphone\"},\"image\":\"" + Imagevalue + "\"}";
                       UTF8Encoding encoding = new UTF8Encoding();
                       HttpWebRequest request = (HttpWebRequest)asyncResult.AsyncState;
                       Stream _body = request.EndGetRequestStream(asyncResult);
                       byte[] formBytes = encoding.GetBytes(envelope);
                       _body.Write(formBytes, 0, formBytes.Length);
                       _body.Close();
                       request.BeginGetResponse(SinUpResponse, request);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
        private void SinUpResponse(IAsyncResult asyncResult)
        {

            try
            {
                int result=0;
                HttpWebRequest request = (HttpWebRequest)asyncResult.AsyncState;
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asyncResult);
                using (Stream data = response.GetResponseStream())
                using (var reader = new StreamReader(data))
                {
                    string text = reader.ReadToEnd();
                    MemoryStream MemoryStream_obj = new MemoryStream(Encoding.Unicode.GetBytes(text));
                    DataContractJsonSerializer DataContractJsonSerializer_obj = new DataContractJsonSerializer(typeof(parseclasss));
                    parseclasss message = DataContractJsonSerializer_obj.ReadObject(MemoryStream_obj) as parseclasss;
                    string _msg = message.response;
                 
                    if (_msg =="Registration Successful!!")
                    {
                        Dispatcher.BeginInvoke(() =>
                            {
                                result = insertDb();
                                if (result == 1)
                                {
                                    MessageBox.Show(text);
                                }
                            });
                    }
                  
                }              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
               
            }
           
        }
        static string Getstring(byte[] abc)
        {
            char[] chars = new char[abc.Length / sizeof(char)];
            System.Buffer.BlockCopy(abc, 0, chars, 0, abc.Length);
            string str = new string(chars);
            return str;
        }
        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

       
     

        public int insertDb()
        {
          
            
                user Registrationmodel_obj = new user();
                Emailvalue = RSAEncrypt(Emailvalue);
                Registrationmodel_obj.email = Emailvalue;
                Registrationmodel_obj.user_name = UsernameValue;
                Passwordvalue = RSAEncrypt(Passwordvalue);
                //Passwordvalue = RSADecrypt(Passwordvalue);
                Registrationmodel_obj.password = Passwordvalue;
                Registrationmodel_obj.phone_type = "Windows phone";
                Registrationmodel_obj.security_question= (lstPickerSecqurityQue.SelectedItem as SecurityQuestion).ID;
                Registrationmodel_obj.answer = Answervalue;
                Registrationmodel_obj.KeepMeLoginTime = new TimeSpan();
                if (!string.IsNullOrEmpty(imagname))
                {
                    IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
                    //myIsolatedStorage.CreateDirectory("imagefolder");
                    string FolderName = "imagefolder";
                    string FilePath = System.IO.Path.Combine(FolderName, imagname);
                    string directoryName = System.IO.Path.GetDirectoryName(FilePath);
                    if (!string.IsNullOrEmpty(directoryName) && !myIsolatedStorage.DirectoryExists(directoryName))
                    {
                        myIsolatedStorage.CreateDirectory(directoryName);
                    }

                    using (IsolatedStorageFileStream fileStream = new IsolatedStorageFileStream(FilePath, System.IO.FileMode.Create, myIsolatedStorage))
                    {
                        
                        //saving jpeg image to isolated storage
                        BitmapImage BitmapImage_obj = new BitmapImage();
                        if (filestream != null)
                        {
                            BitmapImage_obj.SetSource(filestream);
                            WriteableBitmap WriteableBitmap_obj = new WriteableBitmap(BitmapImage_obj);
                            // Encode WriteableBitmap object to a JPEG stream.
                            Extensions.SaveJpeg(WriteableBitmap_obj, fileStream, WriteableBitmap_obj.PixelWidth, WriteableBitmap_obj.PixelHeight, 0, 100);
                        }
                        fileStream.Close();
                    }
                }
                Registrationmodel_obj.profile_pic_name = imagname;
                return RegitrationViewmodel.InserRegistration(Registrationmodel_obj);
           
          
        }

        private void btnclose_Click(object sender, RoutedEventArgs e)
        {
            txtmsg.Text = string.Empty;
            txtmsg.Visibility = Visibility.Collapsed;
            btnclose.Visibility = Visibility.Collapsed;
        }
        private string RSAEncrypt(string value)
        {
            byte[] plaintext = Encoding.Unicode.GetBytes(value);
            CspParameters cspParams = new CspParameters();
            cspParams.KeyContainerName = App.keyvalue;
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(2048, cspParams))
            {
                byte[] encryptedData = RSA.Encrypt(plaintext, false);
                return Getstring(encryptedData);
            }
        }

        private string RSADecrypt(string value)
        {
            byte[] encryptedData = GetBytes(value);
            CspParameters cspParams = new CspParameters();
            cspParams.KeyContainerName = App.keyvalue;
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(2048, cspParams))
            {
                byte[] decryptedData = RSA.Decrypt(encryptedData, false);
                return  Getstring(decryptedData);
            }
        }
        private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
 
            //RetriveQuestion();
            //lstPickerSecqurityQue.ItemsSource = lstcontacts;
        }

   

        private void btnprofile_Click(object sender, RoutedEventArgs e)
        {
            LayoutRoot.IsHitTestVisible = false;
            content.Visibility = Visibility.Visible;
        }

      
        private void txbphoto_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            CameraCaptureTask cameracapturetask = new CameraCaptureTask();
            cameracapturetask.Completed += new EventHandler<PhotoResult>(cameracapturetask_Completed);
            cameracapturetask.Show();
        }
        public void  getfilename(string getvalue)
        {
            if (!String.IsNullOrEmpty(getvalue))
            {

                int position = getvalue.LastIndexOf('\\') + 1;
                int getlength = getvalue.Length;
                int getdiff = getlength - position;
                imagname = getvalue.Substring(position, getdiff);
               
            }

        }
        void cameracapturetask_Completed(object sender, PhotoResult e)
        {
            try
            {
                getfilename(e.OriginalFileName.ToString());
                byte[] bytearray = null;
                BitmapImage BitmapImage_obj = new BitmapImage();
                if (e.TaskResult == TaskResult.OK)
                {
                    filestream = e.ChosenPhoto;
                    using (MemoryStream MemoryStream_obj = new MemoryStream())
                    {
                        BitmapImage_obj.SetSource(e.ChosenPhoto);
                        imgprofile.Source = BitmapImage_obj;
                        if (BitmapImage_obj != null)
                        {
                            WriteableBitmap wbitmp = new WriteableBitmap((BitmapImage)BitmapImage_obj);
                            wbitmp.SaveJpeg(MemoryStream_obj, 120, 50, 0, 100);
                            bytearray = MemoryStream_obj.ToArray();
                        }
                        
                    }
                }
               
                 Imagevalue = Convert.ToBase64String(bytearray);
                 content.Visibility = Visibility.Collapsed;
                 LayoutRoot.IsHitTestVisible = true;
            }
            catch
            {
                MessageBox.Show("Device is not ready for capturing image,please try again");
            }
        }

     

        private void txbgallary_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var photochoosertask = new PhotoChooserTask();
            photochoosertask.Completed += PhotoChooserTaskCompleted;
            photochoosertask.Show();

        }
        void PhotoChooserTaskCompleted(object sender, PhotoResult e)
        {
            try
            {
                getfilename(e.OriginalFileName.ToString());
                byte[] bytearray = null;
                BitmapImage BitmapImage_obj = new BitmapImage();
                if (e.TaskResult == TaskResult.OK)
                {
                       filestream = e.ChosenPhoto;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        BitmapImage_obj.SetSource(e.ChosenPhoto);
                        
                        imgprofile.Source = BitmapImage_obj;
                        if (BitmapImage_obj != null)
                        {
                            WriteableBitmap wbitmp = new WriteableBitmap((BitmapImage)BitmapImage_obj);
                            wbitmp.SaveJpeg(ms,120,50, 0, 100);
                            bytearray = ms.ToArray();
                        }

                    }
                }

                Imagevalue = Convert.ToBase64String(bytearray);
                content.Visibility = Visibility.Collapsed;
                LayoutRoot.IsHitTestVisible = true;
                //bytearray = Convert.FromBase64String(Imagevalue);
                //using (MemoryStream ms = new MemoryStream())
                //{
                //    ms.Write(bytearray, 0, bytearray.Length);
                //    BitmapImage bitmapImage = new BitmapImage();
                //    bitmapImage.SetSource(ms);
                //    imgprofile.Source = bitmapImage;    
                //}
 
            }
            catch
            {
                MessageBox.Show("Device is not ready for capturing image,please try again");
            }
        }

      

        private void txbback_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            content.Visibility = Visibility.Collapsed;
            
        }

      
        private void radgallary_Click(object sender, RoutedEventArgs e)
        {
            var photochoosertask = new PhotoChooserTask();
            photochoosertask.Completed += PhotoChooserTaskCompleted;
            photochoosertask.Show();
        }

     
        private void radcamera_Click(object sender, RoutedEventArgs e)
        {
            CameraCaptureTask cameracapturetask = new CameraCaptureTask();
            cameracapturetask.Completed += new EventHandler<PhotoResult>(cameracapturetask_Completed);
            cameracapturetask.Show();

        }

     

        private void btnstkclose_Click(object sender, RoutedEventArgs e)
        {
            
            content.Visibility = Visibility.Collapsed;
            LayoutRoot.IsHitTestVisible = true;
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
    public class parseclasss
    {
        public string response { get; set; }
    }
}
