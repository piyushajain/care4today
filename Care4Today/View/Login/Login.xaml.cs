﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Care4Today.Model;
using Care4Today.ViewModel;
using System.Text.RegularExpressions;
using Microsoft.Phone.Net.NetworkInformation;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Json;
using C4T.Services;

namespace Care4Today.View.Login
{
    public partial class Login : PhoneApplicationPage
    {
        string DeviceID = "";
        TimeSpan LoginTime;
        string LoginTimeOption = null;
        string Email = "";
        // Constructor --Added by Piyusha
        //Email:kkk@gmail.com
        //password:kkk123

        public Login()
        {
            InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LinkForgetPassword_Click_1(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PasswordLostFocus(object sender, RoutedEventArgs e)
        {
            CheckPasswordWatermark();
            if ((txtEmail.Text != "") && (txtPassword.Password != ""))
            {
                BtnLogin.IsEnabled = true;
            }
            else
            {
                BtnLogin.IsEnabled = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void CheckPasswordWatermark()
        {
            var passwordEmpty = string.IsNullOrEmpty(txtPassword.Password);
            PasswordWatermark.Opacity = passwordEmpty ? 100 : 0;
            txtPassword.Opacity = passwordEmpty ? 0 : 100;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PasswordGotFocus(object sender, RoutedEventArgs e)
        {
            PasswordWatermark.Opacity = 0;
            txtPassword.Opacity = 100;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnLogin_Click_1(object sender, RoutedEventArgs e)
        {
            Match match = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Match(txtEmail.Text);
            bool _isValidPassword = false;
            try
            {
                if (match.Success)
                {
                    if (txtPassword.Password != "")
                    {
                        if ((txtPassword.Password.Any(c => char.IsDigit(c))) && (Regex.IsMatch(txtPassword.Password, @"[!@#$%^&*()_+=\[{\]};:<>|./?,\\'""-]")))
                        {
                            _isValidPassword = true;
                        }
                        if ((txtPassword.Password.Any(c => char.IsLetter(c))) && (Regex.IsMatch(txtPassword.Password, @"[!@#$%^&*()_+=\[{\]};:<>|./?,\\'""-]")))
                        {
                            _isValidPassword = true;
                        }
                        if ((txtPassword.Password.Any(c => char.IsLetter(c))) && (txtPassword.Password.Any(c => char.IsDigit(c))))
                        {
                            _isValidPassword = true;
                        }
                        if (_isValidPassword)
                        {
                            Email = txtEmail.Text;
                            //Check if network available than login with server
                            if (DeviceNetworkInformation.IsNetworkAvailable)
                            {
                                HideError();
                                HttpWebRequest request = HttpWebRequest.CreateHttp("http://192.168.101.137:8181/mhm-web/api/userlogin");
                                request.Accept = "application/json";
                                request.ContentType = "application/json";
                                request.Headers["username"] = txtEmail.Text;
                                request.Headers["password"] = txtPassword.Password;
                                request.Method = "POST";
                                request.BeginGetRequestStream(loginRequest, request);
                            }
                            else
                            {
                                //Localy work with credantial
                                try
                                {
                                    int _userCount = RegitrationViewmodel.ValidatUser(txtEmail.Text, txtPassword.Password);
                                    if (_userCount != 0)
                                    {
                                        user _registrationObj = RegitrationViewmodel.GetUserDetail(txtEmail.Text);
                                        _registrationObj.KeepMeLoginOption = LoginTimeOption;
                                        _registrationObj.KeepMeLoginTime = LoginTime;
                                        _registrationObj.IsActive = "Yes";
                                        int _updateCount = RegitrationViewmodel.UpdateRegister(_registrationObj);
                                    }
                                    else
                                    {
                                        ShowError("Invalid Email or Password");
                                    }

                                }
                                catch (Exception ex)
                                {
                                    ShowError(ex.Message);
                                }
                            }
                        }
                        else
                        {
                            ShowError("Password must be combinations of alphabat,numbers and special symbols");
                        }

                    }
                    else
                    {
                        ShowError("Password can not be blank");

                    }
                }
                else
                {
                    ShowError("Invalid Email format");
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asyncResult"></param>
        private void loginRequest(IAsyncResult asyncResult)
        {
            byte[] id = (byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");
            DeviceID = Convert.ToBase64String(id);
           // DeviceID = "g0A15iVWl32lZEbDW2mx8kuvqL8=";
            string envelope = "{\"deviceId\":\"" + DeviceID + "\",\"source\":\"iphone\"}";
            UTF8Encoding encoding = new UTF8Encoding();
            HttpWebRequest request = (HttpWebRequest)asyncResult.AsyncState;
            Stream _body = request.EndGetRequestStream(asyncResult);
            byte[] formBytes = encoding.GetBytes(envelope);
            _body.Write(formBytes, 0, formBytes.Length);
            _body.Close();
            request.BeginGetResponse(loginResponse, request);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asyncResult"></param>
        private void loginResponse(IAsyncResult asyncResult)
        {
            try
            {
                MemoryStream ms;
                DataContractJsonSerializer _responseData;
                login _loginObj = null;
                HttpWebRequest request = (HttpWebRequest)asyncResult.AsyncState;
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asyncResult);
                HttpWebResponse httpResponse = (HttpWebResponse)response;
                string _responestring = null;
                using (Stream data = response.GetResponseStream())
                using (var reader = new StreamReader(data))
                {

                    _responestring = reader.ReadToEnd();
                    ms = new MemoryStream(Encoding.Unicode.GetBytes(_responestring));
                    _responseData = new DataContractJsonSerializer(typeof(login));
                    _loginObj = _responseData.ReadObject(ms) as login;

                    if (_loginObj.response == "Login Success")
                    {
                        //user _registrationObj = RegitrationViewmodel.GetUserDetail(Email);
                        //_registrationObj.KeepMeLoginOption = LoginTimeOption;
                        //_registrationObj.KeepMeLoginTime = LoginTime;
                        //_registrationObj.IsActive = "Yes";
                        //int _updateCount = RegitrationViewmodel.UpdateRegister(_registrationObj);
                        HideError();
                        //201= First Time login
                        //Fetch DrugDb File From Server unzip it and save locally
                        if (_loginObj.responseCode == 201)
                        {
                            SettingService.GetSettingRequest(Email, txtPassword.Password);

                        }
                        //202= Normal Login
                        //SyncDrugDb 
                        //Call DrugDb Dync Service
                        //Delete Old DrugDb File And Replcae With New Db File
                        else if (_loginObj.responseCode == 202)
                        {
                            SettingService.GetSettingRequest(Email,txtPassword.Password);
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        //Check locally if credanitial available
                    }

                }

            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                        ShowError(text);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCloseError_MouseLeftButtonDown_1(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            HideError();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        private void ShowError(string msg)
        {
            Dispatcher.BeginInvoke(() =>
            {
                GridError.Visibility = Visibility.Visible;
                txtError.Text = msg;
            });

        }

        /// <summary>
        /// 
        /// </summary>
        private void HideError()
        {
            GridError.Visibility = Visibility.Collapsed;
            txtError.Text = "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cancel_btn_MouseLeftButtonDown_1(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ContentPanel.Visibility = Visibility.Visible;
            BorderKeepMe.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toggleKeepmeLogin_Checked_1(object sender, RoutedEventArgs e)
        {
            toggleKeepmeLogin.Content = "On";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toggleKeepmeLogin_Unchecked_1(object sender, RoutedEventArgs e)
        {
            toggleKeepmeLogin.Content = "Off";
            ContentPanel.Visibility = Visibility.Visible;
            BorderKeepMe.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadioBtnAlways_Checked_1(object sender, RoutedEventArgs e)
        {
            LoginTimeOption = "Always";
            LoginTime = new TimeSpan();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadioBtn15Min_Checked_1(object sender, RoutedEventArgs e)
        {
            LoginTime = DateTime.Now.TimeOfDay.Add(TimeSpan.FromMinutes(15));
            LoginTimeOption = "15 minutes";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadioBtn30Min_Checked_1(object sender, RoutedEventArgs e)
        {
            LoginTime = DateTime.Now.TimeOfDay.Add(TimeSpan.FromMinutes(30));
            LoginTimeOption = "30 minutes";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadioBtn1Hour_Checked_1(object sender, RoutedEventArgs e)
        {
            LoginTime = DateTime.Now.TimeOfDay.Add(TimeSpan.FromHours(1));
            LoginTimeOption = "1 hours";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadioBtn3Hour_Checked_1(object sender, RoutedEventArgs e)
        {
            LoginTime = DateTime.Now.TimeOfDay.Add(TimeSpan.FromHours(3));
            LoginTimeOption = "3 hours";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadioBtn12Hour_Checked_1(object sender, RoutedEventArgs e)
        {
            LoginTime = DateTime.Now.TimeOfDay.Add(TimeSpan.FromHours(12));
            LoginTimeOption = "12 hours";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadiobtnNone_Checked_1(object sender, RoutedEventArgs e)
        {
            LoginTime = new TimeSpan();
            LoginTimeOption = "None";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtEmail_LostFocus_1(object sender, RoutedEventArgs e)
        {
            if ((txtEmail.Text != "") && (txtPassword.Password != ""))
            {
                BtnLogin.IsEnabled = true;
            }
            else
            {
                BtnLogin.IsEnabled = false;
            }
        }



        private void btnsignup_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/Login/Termsused.xaml", UriKind.Relative));
        }
    }
}