﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Care4Today.Model
{
  public  class user
    {
       [PrimaryKey, AutoIncrement]
        public int  user_id { get; set; }
        public string user_name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public int security_question { get; set; }
        public string answer { get; set; }
        public string phone { get; set; }
        public string phone_type { get; set; }
        public int  locale_id { get; set; }
        public string locale_name { get; set; }
        public string locale_description { get; set; }
        public int source_id { get; set; }
        public string source_name { get; set; }
        public string profile_pic_name { get; set; }
        public string KeepMeLoginOption { get; set; }
        public TimeSpan KeepMeLoginTime { get; set; }
        public string IsActive { get; set; }
    }
}
