﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Care4Today.Model
{
   public class SecurityQuestion
    {
      [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Question { get; set; }
    }
}
