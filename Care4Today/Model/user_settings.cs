﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Care4Today.Model
{
   public class user_settings
    {
       public int user_setting_id { get; set; }
       public string param_name { get; set; }
       public bool value { get; set; }
    }
}
