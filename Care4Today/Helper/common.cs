﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Care4Today.Helper
{
    class common
    {
        public static List<string> GetLoginTime()
        {
            List<string> TimeValues = new List<string>();
            TimeValues.Add("Always");
            TimeValues.Add("15 minutes");
            TimeValues.Add("30 minutes");
            TimeValues.Add("1 hour");
            TimeValues.Add("3 hours");
            TimeValues.Add("12 hours");
            return TimeValues;
        }
    }
}
